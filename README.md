react-for-beginners

## Notes
- In React, everything is a component. These can be seen as custom HTML tags, which will be rendered with browser default tags.
- To use a React class, import react and extend `React.Component`. A React class needs at least the `render` method to be able to render html.
    - Returning multiline html: wrap it inside a `()`. Reason: JavaScript uses ASI (autimatic semicolon insertion), meaning that without the parentheses, a semicolon will be attached to the `return` statement, ignoring the html following it. Wrapping the multiline html in a semicolon lets JavaScript evaluate it and places the semicolon after the closing parenthese.
    - Sibling elements can not be returned by default (nested elements can). Solution: wrap them in a `React.Fragment` tag.
- To render html to the dom, import react-dom and use its `render` method. The component (class in an html tag) and the element to render it to can be supplied.
- Always import React in separate component files.
- JSX: allows to write html in JavaScript. Under the hood, it calls `React.createElement` with arguments. Certain html attributes are not allowed (reserved JavaScript keywords); for example `class=""` needs to be written as `className=""`.
    - Comments: to write a comment, you need to break out of JSX using `{}`. Inside `{}`, you can write JavaScript comments. `{}` needs to be inside a tag (or use `React.Fragment`).
    - Loop: JSX has no built-in loops, so this must be done in regular JavaScript. When rendering a component in a loop, the component *must* have a prop of `key` with a unique identifier.
        - If you want to access this key in the component, you can't directly do that. Solution: supply this key again but via a different prop, for example `index={<identifier>}`.
- Props:
    - A way to pass data into a component.
    - Props names are made-up, there are no pre-configured props names.
    - They are like attributes on html elements.
    - If you want to pass in anything other than a string, use `{}`.
    - To reference a received prop in a component, use `{this.props.<propname>}`.
        - Stateless Functional Components: when a component only has a render method, it can be rewritten as a function which receives props as its argument. Props are then referenced with `{props.<propname>}`.
- State:
    - Where the data lives.
    - An object that lives inside a component, that holds all the data that the component needs and all of its children may need.
    - An initial state first needs to be declared in the topmost parent component with `state = { <properties>}`
        - https://github.com/tc39/proposal-class-fields
 - The methods that update the state *always* need to live in the same component that holds the initial state property.
    - To update the state, use `this.setState({<property>:<value>})`. This completely overrides the property with the provided value.
    - To be able to call this method within a child component, supply it as a prop.
- React devtools: `$r` is the current component you click on. When you type `$r` in the console, you see that it's an object and you can see all its properties.
- Routing can be done with a separate React module: react-router-dom. Since everything is a component in React, the router is a component too.
    - To add to the url inside of a component, when that component is a child of the router, use `this.props.history.push(<url>`)`;
- When setting a default value on an input field, use `defaultValue` instead of `value`.
- Events: in JSX, eventlisteners are written inline, for example `onClick={this.handleClick}`, which calls the `handClick` method of the component. The `onClick={}` is not rendered in the html.
- React "golden rule": don't touch the rendered dom (querySelector etc.).
- How to reference to an element?
    - Create a property with `React.createRef()` assigned to it.
        - https://github.com/tc39/proposal-class-fields
    - On the element, write `ref={this.<propertyname>}`.
    - In a method, reference it via `this.<propertyname>`.
- All methods that React provides from React.Component bind `this` lexically (to the component). This does not happen automatically in custom methods in a component. Solution: bind custom methods.
    - Solution 1: bind them in the constructor with `.bind(this)`. 
    - Solution 2: rewrite the method as a property with an arrow function assigned to it.
- When you receive the state via a prop, and you modify the contents of the prop, the loops etc. are automatically ran again.
- Inputs: React prevents editing of inputs by default; it hijacks the edited input and sets it to its initial value. To allow input editing, capture the edited input text, and update the state with it.
- You can 'spread' props to a component:
```
const settings = {
	className: 'order',
	timeout: { enter: 500, exit: 500 }
}

<Component { ...settings }></Component>
```
