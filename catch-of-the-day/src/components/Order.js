import React from 'react';
import PropTypes from 'prop-types';
import { formatPrice } from '../helpers';
import { TransitionGroup, CSSTransition } from 'react-transition-group';

class Order extends React.Component {
	static propTypes = {
		fishes: PropTypes.object,
		order: PropTypes.object,
		removeFromOrder: PropTypes.func
	}

	calculateTotal = () => {
		const orderIds = Object.keys(this.props.order); // fish1, fish2, etc.
		const total = orderIds.reduce((prevTotal, key) => {
			const fish = this.props.fishes[key]; // make a match between fish1 in this.props.order and fish1 in this.props.fishes, get its contents
			const count = this.props.order[key]; // get value (how much) of current fish in this.props.order 
			const isAvailable = fish && fish.status === 'available';
			if (isAvailable) {
				return prevTotal + count * fish.price; // multiply amount of fish in this.props.order with the price of that fish in this.props.fishes, add to total
			}
			// fish is not available, skip this calculation
			return prevTotal;
		}, 0);

		return total;
	}

	renderOrder = key => {
		const fish = this.props.fishes[key];
		const count = this.props.order[key];
		const isAvailable = fish && fish.status === 'available';
		const transitionOptions = {
			classNames: 'order',
			key,
			timeout: { enter: 500, exit: 500 }
		}
		
		// make sure the fish is actually there before we continue (state.order is retrieved from localstorage, state.fishes from firebase, the fishes are retrieved later),
		// returning null prevents render
		if (!fish) return null;

		if (!isAvailable) {
			<CSSTransition { ...transitionOptions }>
				return <li key={key}>Sorry {fish ? fish.name : 'fish'} is no longer available</li>;
			</CSSTransition>
		}
		return (
			<CSSTransition { ...transitionOptions }>
				<li key={key}>
					{count} lbs {fish.name} 
					{formatPrice(count * fish.price)}
					<button onClick={() => this.props.removeFromOrder(key)}>&times;</button>
				</li>
			</CSSTransition>
		)
	}

	render() {
		return (
			<div className="order-wrap">
				<h2>Order</h2>
				<TransitionGroup component="ul" className="order">
					{ /* fishes added to order: [fish1, fish4, etc.]  */}
					{Object.keys(this.props.order).map(this.renderOrder)}
				</TransitionGroup>
				<div className="total">
					Total:
					<strong>{formatPrice(this.calculateTotal())}</strong>
				</div>
			</div>
		)
	}
}

export default Order;
