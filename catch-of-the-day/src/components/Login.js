import React from 'react';
import PropType from 'prop-types';

class Login extends React.Component {
	static propTypes = {
		authenticate: PropType.func.isRequired
	}

	render() {
		return (
			<nav className="login">
				<h2>Inventory Login</h2>
				<p>Sign in to manage your store's inventory.</p>
				<button className="github" onClick={() => this.props.authenticate('Github')}>Log In With GitHub</button>
				<button className="facebook" onClick={() => this.props.authenticate('Facebook')}>Log In With Facebook</button>
			</nav>
		)
	}
}


export default Login;
