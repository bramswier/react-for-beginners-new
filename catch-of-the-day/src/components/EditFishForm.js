import React from 'react';
import PropTypes from 'prop-types';

class EditFishForm extends React.Component {
	static propTypes = {
		fish: PropTypes.shape({
			image: PropTypes.string,
			name: PropTypes.string,
			desc: PropTypes.string,
			price: PropTypes.number,
			status: PropTypes.string
		}),
		index: PropTypes.string,
		updateFish: PropTypes.func
	}

	handleChange = event => {
		// update that fish
		// 1. take a copy of the current fish
		const updatedFish = { 
			...this.props.fish, 
			// override property with value, depending on received name attribute and value
			[event.currentTarget.name]: event.currentTarget.value 
		};

		this.props.updateFish(this.props.index, updatedFish);
	}

	render() {
		// Destructure props
		const { image, name, desc, price, status } = this.props.fish;

		return (
			<div className="fish-edit">
				<input name="name" type="text" onChange={this.handleChange} value={name} />
				<input name="price" type="text" onChange={this.handleChange} value={price} />
				<select name="status" onChange={this.handleChange} value={status}>
					<option value="available">Fresh</option>
					<option value="unavailable">Sold Out</option>
				</select>
				<textarea name="desc" onChange={this.handleChange} value={desc}></textarea>
				<input name="image" type="text" onChange={this.handleChange} value={image} />
				<button type="submit" onClick={() => this.props.deleteFish(this.props.index)}>Remove Fish</button>
			</div>
		);
	}
}

export default EditFishForm;
