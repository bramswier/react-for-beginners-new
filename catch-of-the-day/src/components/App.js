import React from 'react';
import PropTypes from 'prop-types';
import Header from './Header';
import Order from './Order';
import Inventory from './Inventory';
import sampleFishes from '../sample-fishes';
import Fish from './Fish';
import base from '../base';

class App extends React.Component {
	state = {
		fishes: {},
		order: {}
	}

	static propTypes = {
		match: PropTypes.object
	}

	componentDidMount() {
		// implicitly received from router
		const { params } = this.props.match;

		// first reinstate our localstorage
		const localStorageRef = localStorage.getItem(params.storeId);
		if (localStorageRef) {
			this.setState({ order: JSON.parse(localStorageRef) });
		}

		// Setup sync of state with firebase
		// `this.ref` not to be confused with React refs
		// Create an entry in the db based on the url, and inside that entry, create a 'fishes' entry to store the fishes
		this.ref = base.syncState(`${params.storeId}/fishes`, {
			context: this,
			state: 'fishes'
		});
	}

	componentDidUpdate() {
		// implicitly received from router
		const { params } = this.props.match;

		// localstorage expects string as value, convert object as JSON to keep object structure inside string
		localStorage.setItem(params.storeId, JSON.stringify(this.state.order))
	}

	// clean up any memory issues
	componentWillUnmount() {
		base.removeBinding(this.ref);
	}

	addFish = fish => {
		// // 1. Take a copy of the existing state
		// const fishes = { ...this.state.fishes };

		// // 2. Add our new fish to that fishes variable
		// fishes[`fish${Date.now()}`] = fish;

		// // 3. Set the new fishes object to state
		// this.setState({ fishes });

		// The above is rewritten: spread existing object properties and add new property (using computed values with `[]`) all at once
		this.setState(prevState => ({
			fishes: { ...prevState.fishes, [`fish${Date.now()}`]: fish }
		}));
	}

	updateFish = (key, updatedFish) => {
		// // 1. Take a copy of the current state
		// const fishes = { ...this.state.fishes };

		// // 2. Update that state
		// fishes[key] = updatedFish;

		// // 3. Set that to state
		// this.setState({ fishes });

		// The above is rewritten
		this.setState(prevState => ({
			fishes: { ...prevState.fishes, [key]: updatedFish }
		}));
	}

	deleteFish = key => {
		const fishes = { ...this.state.fishes };
		fishes[key] = null; // mirorring to firebase needs `null` instead of `delete` property
		this.setState({ fishes });
	}

	loadSampleFishes = () => {
		this.setState({
			fishes: sampleFishes
		})
	}

	addToOrder = key => {
		// // 1. take a copy of state
		// const order = { ...this.state.order };

		// // 2. add to order, or update the number in order
		// order[key] = order[key] + 1 || 1;

		// // 3. call setState to update our state object
		// this.setState({ order });

		// The above is rewritten
		this.setState(prevState => ({
			order: { ...prevState.order, [key]: prevState.order[key] + 1 || 1 }
		}));
	}

	removeFromOrder = key => {
		const order = { ...this.state.order };
		delete order[key];
		this.setState({ order });
	}

	render() {
		return (
			<div className="catch-of-the-day">
				<div className="menu">
					<Header tagline="Fresh Seafood Market" />
					<ul className="fishes">
						{ /* Loop over the fishes property in state. For each item, render a <Fish> component. Each time the state gets updated, this loop will run. */}
						{Object.keys(this.state.fishes).map(key => <Fish key={key} index={key} details={this.state.fishes[key]} addToOrder={this.addToOrder} />)}
					</ul>
				</div>
				<Order fishes={this.state.fishes} order={this.state.order} removeFromOrder={this.removeFromOrder} />
				<Inventory addFish={this.addFish} updateFish={this.updateFish} deleteFish={this.deleteFish} loadSampleFishes={this.loadSampleFishes} fishes={this.state.fishes} storeId={this.props.match.params.storeId} />
			</div>
		)
	}
}

export default App;
