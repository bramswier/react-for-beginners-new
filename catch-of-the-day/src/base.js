import Rebase from 're-base'; // mirror state to firebase
import firebase from 'firebase';

const firebaseApp = firebase.initializeApp({
	apiKey: "AIzaSyCIKS6JqxhARhbAKs8XPl0gUL8YRUENuPk",
	authDomain: "catch-of-the-day-3de97.firebaseapp.com",
	databaseURL: "https://catch-of-the-day-3de97.firebaseio.com"
});

const base = Rebase.createClass(firebaseApp.database());

// This is a named export
export { firebaseApp };

// This is a default export
export default base;
