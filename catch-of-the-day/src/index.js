import React from 'react';
import ReactDOM from 'react-dom';
import Router from './components/Router';
import './css/style.css'; // webpack allows to import css

ReactDOM.render(<Router/>, document.querySelector('#main'));
